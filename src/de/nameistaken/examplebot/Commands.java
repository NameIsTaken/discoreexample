package de.nameistaken.examplebot;

import de.nameistaken.discore.command.message.Command;
import de.nameistaken.discore.command.message.CommandEvent;
import de.nameistaken.discore.command.message.Help;
import de.nameistaken.discore.command.reaction.ReactionCommand;
import de.nameistaken.discore.command.reaction.ReactionCommandEvent;
import de.nameistaken.discore.command.slash.ArgumentDescription;
import de.nameistaken.discore.command.slash.SlashCommand;
import de.nameistaken.discore.command.slash.SlashCommandEvent;
import de.nameistaken.discore.messenger.Emote;
import net.dv8tion.jda.api.entities.User;

/**
 * Klasse mit einigen Commandbeispielen in den verschiedenen Systemen. Würde man normalerweise wohl in mehrere Klassen aufteilen, ist hier für das Beispiel aber ja nicht so wichtig
 */
public class Commands {
    /**
     * CommandMethode für einen Command im alten System. Der Command soll wie folgt genutzt werden können:
     * /userinfo [@User]
     * User ist dabei optional, wenn kein User angegeben ist wird der Autor des Commands verwendet
     *
     * Der Name der Methode ist beliebig wählbar
     *
     * @param event CommandEvent das Informationen zum Event enthält sowie Messenger an den Channel oder den User des Commands. Das CommandEvent MUSS bei JEDEM command
     *              das erste Argument der Methode sein, sonst wird eine Exception geworfen
     * @param user User der als Argument angegeben wurde
     */
    @Command(
            name = "userinfo", //muss festgelegt werden, Hauptname des Commands
            aliasses = "info", //Aliasse, optional, default leer
            permissions = {"bot.admin", "bot.command.userinfo"}, //Permissions, optional, default everyone
            minArgs = 0, //optional, default 0
            maxArgs = 1, //optional, default -1 (= beliebig viele)
            privateChannel = true, //optional, default true, wenn false darf Command nicht im Privatchat mit dem Bot ausgeführt werden
            serverChannel = false, //optional, default false, wenn false darf Command nicht auf Servern ausgeführt werden
            channels = {"123", "456"}, //optional, default leer, wenn nicht leer darf nur in Channeln mit diesen IDs der Command genutzt werden, nur relevant wenn serverchannel = true
            deleteCommandMessage = true //optional, default true, wenn true wird die Command nachricht nach Ausführung gelöscht
    )
    @Help(
            //wie der Name schon sagt, wird hier Information zur Hilfe angegeben. Eine Methode kann mehrere @Help Annotations haben um zB mehrere Subcommands zu erklären
            usage = "userinfo [@User]",
            description = "Gibt Informationen zu dir oder, falls angegeben, dem entsprechenden User aus"
    )
    public void onUserInfo(CommandEvent event, User user){
        if(user == null){
            user = event.getAuthor();
        }
        //... Infos sammeln, MessageEmbed erstellen, dies das
        event.userMessenger().sendMessage("Userinfo bla");
    }

    /**
     * Der gleiche Command nochmal, nur im anderen System (was absolut keinen Sinn ergibt und zu Fehlern führen würde, einen Command in beiden Systemen zu machen, aber nur fürs Beispiel)
     *
     * Der Name der Methode ist beliebig wählbar
     *
     * @param event vergleichbares Event zu CommandEvent, ebenfalls verpflichtend als erstes Argument
     * @param user user
     */
    @SlashCommand(
            parent = "", //optional, default "" (kein Parent), ist für Subcommands/Subcommand Gruppen relevant, mehr dazu im CustomPermissionHandler
            name = "userinfo", //muss festgelegt werden, Hauptname des Commands
            //aliasse gibt es bei slashcommands nicht
            description = "Gibt Informationen zu dir oder, falls angegeben, dem entsprechenden User aus", //Beschreibung des Commands
            permissions = {"bot.admin", "bot.command.userinfo"}, //Permissions, optional, default everyone
            minArgs = 0, //optional, default 0
            maxArgs = 1, //optional, default -1 (= beliebig viele)
            privateChannel = true, //optional, default true, wenn false darf Command nicht im Privatchat mit dem Bot ausgeführt werden, auch wenn der Command als global registriert wurde
            serverChannel = true, //optional, default true, wenn false darf Command nicht auf Servern ausgeführt werden
            channels = {"123", "456"}, //optional, default leer, wenn nicht leer darf nur in Channeln mit diesen IDs der Command genutzt werden, nur relevant wenn serverchannel = true
            useType = true, //muss festgelegt werden, auch für vererbte Commands relevant. Bei einzelnen Commands immer true!
            type = SlashCommand.Type.SERVER, //optional, default SERVER, legt fest ob der Command auf einzelnen Servern oder global (und damit auch im privatchat) ausführbar sein soll
            serverIds = {"321", "654"} //optional, default leer (= default server), Server auf denen der Command registriert wird, falls type nicht global
    )
    @ArgumentDescription(
            //Beschreibung die für das angegebene Argument festgelegt werden soll Eine Methode kann mehrere @ArgumentDescription Annotations haben um alle Parameter zu beschreiben.
            // das sollte auch getan werden. Es gibt zwar default werte die gesetzt werden, aber das ist nicht besonders hilfreich. Zudem praktischer Nebeneffekt dass der Code dadurch indirekt "kommentiert" ist
            parameterIndex = 1, //index des beschriebenen Parameters. Gezählt wird von 0 aufwärts, da aber zunächst das SlashCommandEvent kommt ist der Parameter mit Index 1 der erste für den Command relevante
            description = "User zu dem Informationen ausgegeben werden sollen",
            name = "user" //muss lowercase sein
    )
    public void onUserInfo(SlashCommandEvent event, User user){
        //same shit wie oben
    }

    /**
     * Ein Reactioncommand, keine Ahnung was da ein plausibles Beispiel wäre
     *
     * Der Name der Methode ist beliebig wählbar
     *
     * @param event ReactionCommandEvent. Notwendiger und einziger Parameter der Methode
     */
    @ReactionCommand(
            emotes = {Emote.LETTER_A, Emote.LETTER_B, Emote.LETTER_C}, //emotes bei denen der Command anspringt. Aktuell kann pro emote nur ein Reaction Command festgelegt werden
            reactionType = ReactionCommandEvent.Type.ADD, //optional, default ANY, bei welcher Art reaction spricht der Command an? Nur add, nur remove oder beides?
            removeReaction = true, //optional, default true, soll die Reaction nach dem Command entfernt werden?

            //kram um hoffentlich eine richtige reaction zu validieren
            needsBotMessage = true, //optional, default true, muss die Nachricht, auf die reagiert wird vom Bot sein?
            needsBotReaction = true, //optional, default true, muss der Bot auch mit diesem Emote reacted haben?

            //siehe Command & SlashCommand
            permissions = "bot.reaction.test",
            privateChannel = true,
            serverChannel = true,
            channels = {"..."}
    )
    public void onReaction(ReactionCommandEvent event){
        //Dinge tun. Reaction commands so wenig wie möglich nutzen da das System probleme hat und es vermutlich bessere Alternativen gibt wenn man will
    }

    /**
     * Als letztes ein Beispiel für einen Command, bei dem wir einen custom Typen für eins der Argumente verwenden wollen.
     * In Anwendung könnte das alles mögliche sein, was im Command als ID angegeben wird, wovon wir dann aber im Code ein Objekt erzeugen oder suchen.
     * Auch das können beide Commandsysteme automatisch machen (daher hier keine konkrete Commandannotation, die würde trotzdem gebraucht nach dem gleichen Muster wie sonst auch).
     *
     * @param event SlashCommandEvent oder CommandEvent
     * @param object
     */
    //@SlashCommand(...) / @Command(...)
    public void onTest(SlashCommandEvent event, TestObject object){

    }
}
