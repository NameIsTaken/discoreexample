package de.nameistaken.examplebot;

import de.nameistaken.discore.converter.Converter;
import de.nameistaken.discore.util.Result;

public class TestObject {
    /**
     * Der Converter ist eine Funktion, die ein {@link Result} vom Typ der jeweiligen Klasse aus einem String argument liefert.
     * Wird dazu verwendet um diese Umwandlung automatisch zB vor Commands durchzuführen. Der Converter muss dazu registriert werden
     *
     * @see ExampleBotBuilder#preJDABuild()u
     */
    public static final Converter<TestObject> CONVERTER;

    static {
        CONVERTER = stringArgument -> {
            Result<TestObject> result;

            //verarbeitung des String arguments
            boolean couldBeConverted = true;
            if (couldBeConverted) {
                //Beispiel für Rückgabe eines gültigen Results nach erfolgreicher Umwandlung
                result = new Result<>(new TestObject(1, stringArgument, true));
            } else {
                //Beispiel für Rückgabe eines nicht gültigen Results nachdem das argument nicht umgewandelt werden konnte
                result = new Result<>();
                result.setErrorMessage("Argument kann nicht zum Typ TestObject umgewandelt werden");
                //Achtung! Diese Fehlermeldung wird dem User angezeigt, wenn das Argument nicht umgewandelt werden kann
            }

            return result;
        };
    }

    private int id;
    private String value;
    private boolean someOtherValue;

    /**
     * ist wirklich nur ein Beispiel, hier kann alles mögliche sein und umgewandelt werden
     */
    public TestObject(int id, String value, boolean someOtherValue) {
        this.id = id;
        this.value = value;
        this.someOtherValue = someOtherValue;
    }
}
