package de.nameistaken.examplebot;

import de.nameistaken.discore.command.slash.ArgumentDescription;
import de.nameistaken.discore.command.slash.SlashCommand;
import de.nameistaken.discore.command.slash.SlashCommandEvent;
import de.nameistaken.discore.permission.PermissionHandler;
import net.dv8tion.jda.api.entities.User;

/**
 * Ein PermissionHandler muss festgelegt werden um an verschiedenen Stellen automatisch Permissions prüfen zu können (zB bei der Ausführung eines Commands).
 *
 * @see de.nameistaken.discore.permission.PermissionManager zur Registrierung des Handlers sowie um selbst permissions zu prüfen
 */
public class CustomPermissionHandler extends PermissionHandler {
    /**
     * @param user       Nutzer dessen Permission geprüft werden soll
     * @param permission Permission auf die User geprüft werden soll
     * @return true, falls der User die permission hat, false sonst
     * @see PermissionHandler#EVERYONE_PERMISSION default permission, die immer true zurück gibt, unabhängig von der Implementierung des PermissionHandlers
     */
    @Override
    protected boolean checkPermission(User user, String permission) {
        //hier kann was auch immer passieren, was notwendig ist um die Permission des Nutzers zu checken. Datenbankzugriff, Zugriff auf eine Datei,
        //auslesen der ID des Users, finden und prüfen der Rollen des Users auf einem bestimmten Server, ...
        return true;
    }

    /**
     * Wir wollen eine Reihe an Commands/Subcommands, die wie folgt aussehen:
     * /permissions user add [user] [permission]
     * /permissions user remove [user] [permission]
     * /permissions user list [user]
     * /permissions reload
     * später können dann zB commands wie /permissions role add ... dazu kommen.
     * Wir legen nun also die Commands add, remove, list als eigene Methoden fest und geben jeweils permissions/user als parent an, bei update nur permissions als parent
     *
     * Durch diese Verwendung können die jeweiligen super commands nicht mehr als eigenständige commands verwendet werden:
     * /permissions und /permissions user können also nicht mehr als Commands registriert werden (Discord Vorgabe)
     */
    @SlashCommand(
            parent = "permissions/user",
            name = "add",
            description = "Fügt dem User die angegebene Permission hinzu",
            minArgs = 1,
            maxArgs = 1,
            permissions = "bot.admin",
            serverChannel = false,

            //wir nutzen hier den typ, das heißt diese Informationen werden für den gesamten /permissions Command mit allen möglichen Subcommands verwendet
            //bei allen anderen geben wir also useType = false an. Wird mehrfach true angegeben kann nicht garantiert werden, welche Daten genutzt werden
            useType = true,
            type = SlashCommand.Type.GLOBAL,
            serverIds = {} //wird hier wegen global sowieso ignoriert, gehört aber auch zu dem Informationsblock, der durch useType = true beachtet wird
    )
    @ArgumentDescription(parameterIndex = 1, description = "User der die Permission erhalten soll", name = "target")
    @ArgumentDescription(parameterIndex = 2, description = "Permission die der User erhält", name = "permission")
    public void addUserPermission(SlashCommandEvent event, User user, String permission) {
        //wie das dann genau aussieht interessiert nicht wirklich
        System.out.printf("permissions user add %s %s", user.getAsTag(), permission);
    }

    @SlashCommand(
            parent = "permissions/user",
            name = "remove",
            description = "Entfernt die angegebene Permission vom User",
            minArgs = 2,
            maxArgs = 2,
            permissions = "bot.admin",
            serverChannel = false,
            useType = false
    )
    @ArgumentDescription(parameterIndex = 1, description = "User dessen Permission entfernt werden soll", name = "target")
    @ArgumentDescription(parameterIndex = 2, description = "Permission die dem User entfernt wird", name = "permission")
    public void removeUserPermission(SlashCommandEvent event, User user, String permission) {
        //wie das dann genau aussieht interessiert nicht wirklich
        System.out.printf("permissions user remove %s %s", user.getAsTag(), permission);
    }

    @SlashCommand(
            parent = "permissions/user",
            name = "list",
            description = "Listet alle Permissions des Users",
            minArgs = 2,
            maxArgs = 2,
            permissions = "bot.admin",
            serverChannel = false,
            useType = false
    )
    @ArgumentDescription(parameterIndex = 1, description = "User dessen Permissions gelistet werden sollen", name = "target")
    public void listUserPermissions(SlashCommandEvent event, User user) {
        //wie das dann genau aussieht interessiert nicht wirklich
        System.out.printf("permissions user list %s", user.getAsTag());
    }

    @SlashCommand(
            parent = "permissions",
            name = "reload",
            description = "Lädt die Permissions neu aus der Datenbank",
            maxArgs = 0,
            permissions = "bot.admin",
            serverChannel = false,
            useType = false
    )
    public void reload(SlashCommandEvent event) {
        //wie das dann genau aussieht interessiert nicht wirklich
        System.out.println("permissions reload");
    }
}
