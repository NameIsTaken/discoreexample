package de.nameistaken.examplebot;

import de.nameistaken.discore.bot.DiscordBot;
import de.nameistaken.discore.bot.DiscordBotBuilder;
import de.nameistaken.discore.command.preset.Botinfo;
import de.nameistaken.discore.converter.ConverterManager;
import de.nameistaken.discore.permission.PermissionManager;

/**
 * {@link DiscordBotBuilder} ist die zentrale Klasse zum bauen und starten des Discord Bots. Die Library ist darauf ausgelegt, dass
 * eine einzelne Bot Instanz erstellt und benutzt wird. Durch den Aufruf {@link DiscordBotBuilder#build()} wird diese Instanz erstellt und eine Reihe von
 * teils vordefinierten, teils anpassbaren Prozessen abgespielt. Dazu gehören die drei implementierbaren Methoden {@link DiscordBotBuilder#preJDABuild()},
 * {@link DiscordBotBuilder#postJDABuild(DiscordBot)} und {@link DiscordBotBuilder#postBotStart(DiscordBot)}. Intern wird neben dem Bauen der JDA und der Instanz des
 * {@link DiscordBot} auch das registrieren von Listenern, Slashcommands usw. vorgenommen. Der gebaute DiscordBot wird von {@link DiscordBotBuilder#build()} zurückgegeben oder kann durch
 * {@link DiscordBot#getInstance()} abgerufen werden
 */
public class ExampleBotBuilder extends DiscordBotBuilder {
    /**
     * @param botToken discord token für den Bot
     * @param commandPrefix Prefix für commands (zB "!", "/", "?b"). Gilt nur für Commandsystem und nicht für Slashcommands
     * @param defaultServerId ID die als default Server festgelegt werden soll. Empfohlen, aber nicht notwendig (darf null sein)
     * @param forcePrefix wenn true muss auch im Privatchat mit dem Bot ein Commandprefix angegeben werden. Gilt nur für Commandsystem und nicht für Slashcommands
     * @param deleteUnknown wenn true werden Nachrichten, die mit dem commandPrefix anfangen, aber nicht als command erkannt werden, gelöscht
     */
    public ExampleBotBuilder(String botToken, String commandPrefix, String defaultServerId, boolean forcePrefix, boolean deleteUnknown) {
        super(botToken, commandPrefix, defaultServerId, forcePrefix, deleteUnknown);
    }

    /**
     * Diese Methode wird intern als erstes aufgerufen. Hier sollten also alle Aktionen durchgeführt werden, die für das erfolgreiche Starten des Bots nötig sind.
     * Dazu gehören das registrieren von Listenern, Slashcommands, Commands, ReactionCommands, das festlegen eines PermissionHandlers und ggf projektspezifische eigene Werte
     *
     * @see #register(Class, Object, String...) die register Methoden können für alle zu registrierenden Listener/Commands/... verwendet werden. Wird in einer Klasse also sowohl ein Command als auch ein Listener
     *  verwendet muss diese nur einmal registriert werden
     */
    @Override
    protected void preJDABuild() {
        ConverterManager.registerConverter(TestObject.class, TestObject.CONVERTER); //muss vor dem registrieren der Commands passieren, die diesen Converter nutzen, da sonst Fehler auftreten
        //mit ConverterManager.isConverterAvailable(class) kann geprüft werden, ob es einen Converter für die jeweilige Klasse gibt.
        //Standardmäßig gibt es Converter für alle primitiven Datentypen sowie ihre entsprechenden Klassen (also zB boolean & Boolean), String,
        // Timestamp, TextChannel, Role, User, Message und Emote (sowohl de.nameistaken.discore.util.Emote als auch net.dv8tion.jda.api.entities.Emote)

        CustomPermissionHandler handler = new CustomPermissionHandler();
        PermissionManager.setHandler(handler); //muss festgelegt werden, sonst läuft der Bot früher oder später in Fehler

        register(MemberListener.class);
        register(Commands.class);

        //intern wird von der angegebenen Klasse per reflection eine instanz erzeugt. Da wir hier schon eine Instanz haben (handler) können wir dem Programm den schritt abnehmen und sie mit angeben
        register(CustomPermissionHandler.class, handler);

        //es gibt einige vorgefertigte Commands, die allerdings im alten System sind + default mäßig für jeden nutzbar sind, die sollten also mit vorsicht verwendet und
        // nur mit angegebener permission registriert werden
        register(Botinfo.class, null, "bot.admin");
    }

    /**
     * Diese Methode wird nach dem Bauen der JDA (und damit dem Start des Bots) aufgerufen. zu diesem Zeitpunkt werden sänmtliche Events noch zurückgehalten.
     * Diese Methode eignet sich also dazu, all die Dinge zu initialisieren, die eine Verbindung zu Discord benötigen und ohne die Commands/Listener etc nicht funktionieren.
     *
     * @param discordBot der zu dem Zeitpunkt erstellte Discordbot. Identisch mit {@link DiscordBot#getInstance()} zum gleichen Zeitpunkt
     */
    @Override
    protected void postJDABuild(DiscordBot discordBot) {
        MemberListener.initChannel();
    }

    /**
     * Wenn diese Methode aufgerufen wird läuft der Bot vollständig, alle bis dahin zurückgehaltenen Events wurden nachträglich bearbeitet und neue Events werden direkt verarbeitet.
     * Achtung: solange diese Methode nicht abgearbeitet wurde, ist {@link DiscordBotBuilder#build()} nicht fertig.
     *
     * @param discordBot der zu dem Zeitpunkt erstellte Discordbot. Identisch mit {@link DiscordBot#getInstance()} zum gleichen Zeitpunkt
     */
    @Override
    protected void postBotStart(DiscordBot discordBot) {

    }
}
