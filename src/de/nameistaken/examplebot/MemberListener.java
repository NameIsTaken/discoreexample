package de.nameistaken.examplebot;

import de.nameistaken.discore.bot.DiscordBot;
import de.nameistaken.discore.embeder.MessageEmbeder;
import de.nameistaken.discore.events.EventListener;
import de.nameistaken.discore.messenger.*;
import de.nameistaken.discore.messenger.Emote;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberRemoveEvent;
import net.dv8tion.jda.api.interactions.InteractionHook;

/**
 * Ein einfacher Listener, der joins und leaves in einen bestimmten Channel schreibt.
 * Beispiel für ein
 */
public class MemberListener {
    /**
     * Messenger sind ein Tool aus der Library mit dem man einfacher Nachrichten senden kann. Es gibt eigene Messenger für TextChannel bzw MessageChannel {@link ChannelMessenger},
     * Interactions {@link InteractionMessenger}, User (Privatchannel) {@link UserMessenger} sowie um auf Nachrichten zu antworten {@link ReplyMessenger}. Diese können jeweils einzeln per Konstruktor erstellt oder per
     * {@link Messenger#get(MessageChannel)}, {@link Messenger#get(User)}, {@link Messenger#get(InteractionHook)}, {@link Messenger#get(Message)} generiert werden (die geben zwar nur einen abstrakten Messenger zurück,
     * die Subklassen haben aber sowieso keine eigenen Methoden und so muss man sich keine Klassennamen merken ¯\_(ツ)_/¯)
     *
     * Neben einfacherem senden von Nachrichten ermöglichen Messenger auch das Senden eines {@link de.nameistaken.discore.embeder.MessageEmbeder}, der wiederum ein Tool zum vereinfachten Bauen von Embed Nachrichten ist
     *
     * @see Messenger
     */
    private static Messenger memberlogMessenger;

    public static void initChannel(){
        TextChannel memberlogChannel = DiscordBot.getInstance().getDefaultServer().getTextChannelById("1234");
        memberlogMessenger = new ChannelMessenger(memberlogChannel);

        //alternativ:
        memberlogMessenger = Messenger.get(memberlogChannel);
    }

    @EventListener
    public void onJoin(GuildMemberJoinEvent event){
        //einfache Nachricht
        memberlogMessenger.sendMessage(event.getMember().getAsMention() + " joined.");

        //alternative mit Embed
        MessageEmbeder embeder = new MessageEmbeder();
        embeder.setTitle("New Server Member!")
                .append(event.getMember().getAsMention())
                .append(" joined.")
                .setFooter("Welcome!")
                .green();
        memberlogMessenger.sendMessage(embeder);

        //Nachricht senden und danach eine Reaction adden
        memberlogMessenger.sendMessageAndThen(
                embeder,
                message -> message.addReaction(Emote.LAUGHING.toString()).queue()
        );

        //Nachricht senden und eine Exception werfen falls die Nachricht nicht gesendet werden konnte
        memberlogMessenger.sendMessageOrElse(
                embeder,
                error -> {
                    throw new IllegalStateException(error);
                }
        );

        //Nachricht senden, bei Erfolg Reaction adden, bei Fehler Exception werfen
        memberlogMessenger.sendMessage(
                embeder,
                message -> message.addReaction(Emote.LAUGHING.toString()).queue(),
                error -> {
                    throw new IllegalStateException(error);
                }
        );

        //falls man das JDA MessageEmbed braucht, weil man eine JDA Funktion verwenden will (zB eine Nachricht editieren)
        MessageEmbed embed = embeder.build();
    }

    @EventListener
    public void onLeave(GuildMemberRemoveEvent event){
        memberlogMessenger.sendMessage(event.getUser().getAsTag() + " left.");
    }
}
